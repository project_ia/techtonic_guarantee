import React from 'react';
import './styles/Botonesmenu.css';
import { Collapse, CardBody, Card, Col, Row, Container } from 'reactstrap';

export default class Home extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            current: [],
            abierto: false,
        }
        this.goMenu = this.goMenu.bind(this);
        this.cerrar = this.cerrar.bind(this);
    }

    goMenu() {
        window.location.href = 'https://tech-ai.herokuapp.com/menu';
    }
    cerrar() {
        this.setState({
            abierto: !this.state.abierto
        })
    }
    render() {
        const toggle = () => this.cerrar();

        const img = {
            width: "235px",
            height: "300px",
            borderRadius: "1000px",
        }
        const img2 = {
            width: "60px",
            height: "60px",
        }
        const letra ={
            fontSize: "18px",
            textAlign: "justify",
            marginTop: "25px"
        }
        const titulo={
            marginTop: "100px"
        }
        const hr = {
            border: "0",
            width: "80%",
            color: "black",
            backgroundColor: "black",
            height: "1px"
          }
        return (
            <>
                <br /><br /><br />
                <div>
                    <button onClick={this.goMenu} className="botonesMenu">EMPEZAR</button>{' '}
                </div>
                <br />
                <div>
                    <button onClick={toggle} className="botonesMenu">CONÓCENOS</button>{' '}
                    <Collapse style={{padding:"10px"}} isOpen={this.state.abierto}>
                        <Card>
                            <CardBody>
                                <Container>
                                <Row>
                                    <Col> <img style={img} src={"/img/antu.png"} />
                                    </Col>
                                    <Col><h3 style={titulo}>Antu Killén Pangue Bustamante</h3> 
                                    <br/>
                                    <a href="https://www.linkedin.com/in/antu-kill%C3%A9n-pangue-bustamante/"><img style={img2} src={"/img/linkedin.png"}/></a>
                                    </Col>
                                    <Col><p style={letra}>
                                        Graduado en Nanociencia y Nanotecnología por la UAB,
                                        donde descubri mi pasión por la programación. 
                                        Una vez terminada la carrera, continué formandome 
                                        en este increíble ámbito de la programación, en concreto,
                                        en IA y Machine Learning con Python.
                                        </p></Col>
                                </Row>
                                
                                <hr style={hr}></hr>
                                <Row>

                                    <Col> <img style={img} src={"/img/roberto.png"} /></Col>
                                        <Col><h3 style={titulo}>Roberto Garcia Baz</h3>
                                        <br/>
                                        <a href="https://www.linkedin.com/in/roberto-garcia-programmer-environment/"><img style={img2} src={"/img/linkedin.png"}/></a>
                                        </Col>
                                        <Col>
                                        <p style={letra}>
                                        Graduado en Geología por la UAB especializado 
                                        en medio ambiente. Tras varios años ejerciendo, me di cuenta que la programación 
                                        podía ser una herramienta que complementaría mi perfil, actualmente me especializo 
                                        en IA y machine learning con python por su aplicación directa en las ciencias.
                                        </p></Col>
                                    </Row>
                                    
                                    <hr style={hr}></hr>
                                    <Row>

                                            <Col><img style={img} src={"/img/patricia.png"} />
                                            </Col>
                                            <Col><h3 style={titulo}>Patricia Montecino Ferreira</h3>
                                            <br/>
                                            <a href="https://www.linkedin.com/in/patriciamontecinoferreira/"><img style={img2} src={"/img/linkedin.png"}/></a>
                                            </Col>
                                            <Col>
                                            <p style={letra}>
                                            Graduada en Criminología por la UB di un giro 
                                            a mi carrera profesional introduciendome en el ámbito de la programación y 
                                            especializándome en inteligencia artificial y machine learning.
                                            Soy una persona que sintetiza y une dos mundos muy diferentes consiguiendo un 
                                            punto de vista heterogéneo al del perfil tradicional de programador.
                                    </p></Col>
                                    </Row>
                                    
                                    <hr style={hr}></hr>
                                    <Row>

                                        <Col><img style={img} src={"/img/marcel.png"} /></Col>
                                        <Col><h3 style={titulo}> Marcel Luque Gené</h3>
                                        <br/>
                                        <a href="https://www.linkedin.com/in/marcelluquegene/"><img style={img2} src={"/img/linkedin.png"}/></a>
                                        </Col>
                                            <Col>
                                            <p style={letra}>
                                            Graduado en Geología por la UAB especializado en GIS.
                                             Decidí cambiar mi ámbito profesional al sector de la programación proporcionándome 
                                             un perfil más versátil, técnico y científico. En mi afán de seguir adquiriendo 
                                             conocimientos y dar valor a éstos, actualmente me especializo en inteligencia artificial y machine learning así como en Data analysis.
                                            </p></Col>
                                    </Row>
                                    </Container>
                            </CardBody>

                        </Card>
                    </Collapse>
                </div>

            </>
        )
    }
}