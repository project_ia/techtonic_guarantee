import React, { Component } from 'react';
import './styles/Botonesmenu.css';
import { Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';

const HEADERS = new Headers({
  'Accept': 'application/json',
  'Content-Type': 'application/json'
});

class Formulario extends Component {
  constructor(props) {
    super(props)
    this.state = {
      mostrar: false,
      estimation: [],
      land_surface_condition: '',
      foundation_type: '',
      roof_type: '',
      ground_floor_type: '',
      other_floor_type: '',
      position: '',
      plan_configuration: '',
      has_superstructure_adobe_mud: 0,
      has_superstructure_mud_mortar_stone: 0,
      has_superstructure_stone_flag: 0,
      has_superstructure_cement_mortar_stone: 0,
      has_superstructure_mud_mortar_brick: 0,
      has_superstructure_cement_mortar_brick: 0,
      has_superstructure_timber: 0,
      has_superstructure_bamboo: 0,
      has_superstructure_rc_non_engineered: 0,
      has_superstructure_rc_engineered: 0,
      has_superstructure_other: 0,
      has_secondary_use_agriculture: 0,
      has_secondary_use_hotel: 0,
      has_secondary_use_rental: 0,
      has_secondary_use_institution: 0,
      has_secondary_use_school: 0,
      has_secondary_use_industry: 0,
      has_secondary_use_health_post: 0,
      has_secondary_use_gov_office: 0,
      has_secondary_use_use_police: 0,
      has_secondary_use_other: 0,
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSecondaryUse = this.handleSecondaryUse.bind(this);
    this.calcularEstimacion = this.calcularEstimacion.bind(this);
  }

  handleChange = (event) => {
    const value = event.target.value;
    const name = event.target.name;
    this.setState({
      [name]: value
    });
  }
  handleSecondaryUse = (event) => {
    const value = event.target.value;
    this.setState({
      [value]: 1
    });
  }

  calcularEstimacion() {

    const building = {
      land_surface_condition: this.state.land_surface_condition,
      foundation_type: this.state.foundation_type,
      roof_type: this.state.roof_type,
      ground_floor_type: this.state.ground_floor_type,
      other_floor_type: this.state.other_floor_type,
      position: this.state.position,
      plan_configuration: this.state.plan_configuration,
      has_superstructure_adobe_mud: this.state.has_superstructure_adobe_mud,
      has_superstructure_mud_mortar_stone: this.state.has_superstructure_mud_mortar_stone,
      has_superstructure_stone_flag: this.state.has_superstructure_stone_flag,
      has_superstructure_cement_mortar_stone: this.state.has_superstructure_cement_mortar_stone,
      has_superstructure_mud_mortar_brick: this.state.has_superstructure_mud_mortar_brick,
      has_superstructure_cement_mortar_brick: this.state.has_superstructure_cement_mortar_brick,
      has_superstructure_timber: this.state.has_superstructure_timber,
      has_superstructure_bamboo: this.state.has_superstructure_bamboo,
      has_superstructure_rc_non_engineered: this.state.has_superstructure_rc_non_engineered,
      has_superstructure_rc_engineered: this.state.has_superstructure_rc_engineered,
      has_superstructure_other: this.state.has_superstructure_other,
      has_secondary_use_agriculture: this.state.has_secondary_use_agriculture,
      has_secondary_use_hotel: this.state.has_secondary_use_hotel,
      has_secondary_use_rental: this.state.has_secondary_use_rental,
      has_secondary_use_institution: this.state.has_secondary_use_institution,
      has_secondary_use_school: this.state.has_secondary_use_school,
      has_secondary_use_industry: this.state.has_secondary_use_industry,
      has_secondary_use_health_post: this.state.has_secondary_use_health_post,
      has_secondary_use_gov_office: this.state.has_secondary_use_gov_office,
      has_secondary_use_use_police: this.state.has_secondary_use_use_police,
      has_secondary_use_other: this.state.has_secondary_use_other,
    };
    console.log(building)

    const opcions = {
      method: "POST",
      headers: HEADERS,
      body: JSON.stringify(building)
    };

    const calcularURL = "https://techtonic-api.herokuapp.com/calcular/";

    fetch(calcularURL, opcions)
      .then(texto => texto.json())
      .then(datos => this.setState({
        estimation: datos,
        mostrar: true
      }, () => console.log('estimacion', this.state.estimation)))
      .catch(error => console.log("se ha producido un error: ", error));
  }

  render() {

    function refreshPage() {
      window.location.reload(false);
    }

    function goMenu() {
      window.location.href = 'https://tech-ai.herokuapp.com/menu'
    }
    const container = {
      backgroundColor: "white",
      marginTop: "101px",
      borderRadius: "10px",
      padding: "10px"

    }

    const containerResult = {
      backgroundColor: "white",
      borderRadius: "10px",
      padding: "10px"

    }
    const titulos = {
      fontSize: "20px",
      fontFamily: "monospace",
      color: "rgb(83, 83, 83)",
      letterSpacing: "5px"
    }
    const campos = {
      fontSize: "10px"
    }
    const input = {
      height: "30px",
      fontSize: "10px",

    }
    const hr = {
      border: "0",
      width: "100%",
      color: "black",
      backgroundColor: "black",
      height: "5px"
    }

    const label = {
      fontFamily: "monospace",
      color: "rgb(83, 83, 83)",
      letterSpacing: "2px"
    }

    const h2 = {
      fontFamily: "monospace",
      color: "black",
      letterSpacing: "2px"
    }

    return (
      <div style={{ padding: "1%" }}>
        <Container style={container}>
          <h2 style={h2}>NUEVO REGISTRO</h2>
          <hr style={hr}></hr>
          <Row>
            <Col xs="6" style={titulos}>Superfície<hr></hr></Col>
            <Col xs="6" style={titulos}>Estructura<hr></hr></Col>
          </Row>
          <Form>
            <Row>
              <Col xs="6" style={campos}>
                <FormGroup>
                  <Label style={label}>Condición de la superfície terrestre</Label>
                  <Input
                    onChange={this.handleChange}
                    style={input}
                    type="select"
                    name="land_surface_condition"
                    id="exampleSelect"
                  >
                    <option></option>
                    <option value="Flat" >Plana</option>
                    <option value="Moderate slope" >Pendiente moderada</option>
                    <option value="Steep slope" >Pendiente pronunciada</option>
                  </Input>
                </FormGroup>
              </Col>
              <Col xs="6" style={campos}>
                <FormGroup check inline>
                  <Label style={label} check>
                    <Input
                      onChange={(e) => {
                        this.handleChange({
                          target: {
                            name: e.target.name,
                            value: e.target.value,
                          },
                        });
                      }}
                      type="checkbox"
                      name="has_superstructure_adobe_mud"
                      value={1}
                    /> Barro y adobe
                  </Label>
                </FormGroup>
                <br />
                <br />
                <FormGroup check inline>
                  <Label style={label} check>
                    <Input
                      onChange={(e) => {
                        this.handleChange({
                          target: {
                            name: e.target.name,
                            value: e.target.value,
                          },
                        });
                      }}
                      type="checkbox"
                      name="has_superstructure_mud_mortar_stone"
                      value={1}
                    /> Mortero y barro y piedra
                  </Label>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs="6" style={campos}>
                <FormGroup>
                  <Label style={label}>Plano de la configuración</Label>
                  <Input
                    onChange={this.handleChange}
                    style={input}
                    type="select"
                    name="plan_configuration"
                    id="exampleSelect"
                  >
                    <option></option>
                    <option value="Rectangular">Rectangular</option>
                    <option value="L-shape">Forma de L</option>
                    <option value="Square">Cuadrado</option>
                    <option value="T-shape">Forma de T</option>
                    <option value="Multi-projected">Multiproyectado</option>
                    <option value="H-shape">Forma de H</option>
                    <option value="U-shape">Forma de U</option>
                    <option value="Others">Otros</option>
                    <option value="E-shape">Forma de E</option>
                    <option value="Building with Central Courtyard">Edificio con patio central</option>
                  </Input>
                </FormGroup>

              </Col>
              <Col xs="6" style={campos}>
                <FormGroup check inline>
                  <Label style={label} check>
                    <Input
                      onChange={(e) => {
                        this.handleChange({
                          target: {
                            name: e.target.name,
                            value: e.target.value,
                          },
                        });
                      }}
                      type="checkbox"
                      name="has_superstructure_stone_flag"
                      value={1}
                    /> Losa
                  </Label>
                </FormGroup>
                <br />
                <br />
                <FormGroup check inline>
                  <Label style={label} check>
                    <Input
                      onChange={(e) => {
                        this.handleChange({
                          target: {
                            name: e.target.name,
                            value: e.target.value,
                          },
                        });
                      }}
                      type="checkbox"
                      name="has_superstructure_cement_mortar_stone"
                      value={1}
                    /> Mortero y cemento
                  </Label>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs="6" style={campos}>
                <FormGroup>
                  <Label style={label}>Posición</Label>
                  <Input
                    onChange={this.handleChange}
                    style={input}
                    type="select"
                    name="position"
                    id="exampleSelect"
                  >
                    <option></option>
                    <option value="Not attached">No Unido</option>
                    <option value="Attached-1 side">Unido- 1 lado</option>
                    <option value="Attached-2 side">Unido- 2 lado</option>
                    <option value="Attached-3 side">Unido- 3 lado</option>
                  </Input>
                </FormGroup>
              </Col>
              <Col xs="6" style={campos}>
                <FormGroup check inline>
                  <Label style={label} check>
                    <Input
                      onChange={(e) => {
                        this.handleChange({
                          target: {
                            name: e.target.name,
                            value: e.target.value,
                          },
                        });
                      }}
                      type="checkbox"
                      name="has_superstructure_mud_mortar_brick"
                      value={1}
                    /> Barro y ladrillo
                  </Label>
                </FormGroup>
                <br />
                <br />
                <FormGroup check inline>
                  <Label style={label} check>
                    <Input
                      onChange={(e) => {
                        this.handleChange({
                          target: {
                            name: e.target.name,
                            value: e.target.value,
                          },
                        });
                      }}
                      type="checkbox"
                      name="has_superstructure_timber"
                      value={1}
                    /> Madera
                  </Label>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs="3" style={campos}>
                <FormGroup>
                  <Label style={label}>Cimientos</Label>
                  <Input
                    onChange={this.handleChange}
                    style={input}
                    type="select"
                    name="foundation_type"
                    id="exampleSelect"
                  >
                    <option></option>
                    <option value="Other">Otros</option>
                    <option value="Mud mortar-Stone/Brick">Mortero y barro y piedra/Ladrillo</option>
                    <option value="Cement-Stone/Brick">Cemento/Ladrillo</option>
                    <option value="Bamboo/Timber">Bambú/Madera</option>
                    <option value="RC">RC</option>
                  </Input>
                </FormGroup>
              </Col>
              <Col xs="3" style={campos}>
                <FormGroup>
                  <Label style={label}>Tejado</Label>
                  <Input
                    onChange={this.handleChange}
                    style={input}
                    type="select"
                    name="roof_type"
                    id="exampleSelect"
                  >
                    <option></option>
                    <option value="Bamboo/Timber-Light roof">Bambú/Madera ligera</option>
                    <option value="Bamboo/Timber-Heavy roof">Bambú/Madera pesada</option>
                    <option value="RCC/RB/RBC">RCC/RB/RBC</option>
                  </Input>
                </FormGroup>
              </Col>
              <Col xs="6" style={campos}>
                <FormGroup check inline>
                  <Label style={label} check>
                    <Input
                      onChange={(e) => {
                        this.handleChange({
                          target: {
                            name: e.target.name,
                            value: e.target.value,
                          },
                        });
                      }}
                      type="checkbox"
                      name="has_superstructure_rc_engineered"
                      value={1}
                    /> RC_engineered
                  </Label>
                </FormGroup>
                <br />
                <br />
                <FormGroup check inline>
                  <Label style={label} check>
                    <Input
                      onChange={(e) => {
                        this.handleChange({
                          target: {
                            name: e.target.name,
                            value: e.target.value,
                          },
                        });
                      }}
                      type="checkbox"
                      name="has_superstructure_rc_non_engineered"
                      value={1}
                    /> RC_non_engineered
                  </Label>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs="3" style={campos}>
                <FormGroup>
                  <Label style={label}>Suelo</Label>
                  <Input
                    onChange={this.handleChange}
                    style={input}
                    type="select"
                    name="ground_floor_type"
                    id="exampleSelect"
                  >
                    <option></option>
                    <option value="Mud">Barro</option>
                    <option value="Brick/Stone">Ladrillo/Piedra</option>
                    <option value="RC">RC</option>
                    <option value="Timber">Madera</option>
                    <option value="Other">Otros</option>
                  </Input>
                </FormGroup>
              </Col>
              <Col xs="3" style={campos}>
                <FormGroup>
                  <Label style={label}>Otros suelos</Label>
                  <Input
                    onChange={this.handleChange}
                    style={input}
                    type="select"
                    name="other_floor_type"
                    id="exampleSelect"
                  >
                    <option></option>
                    <option value="Not applicable">No apicable</option>
                    <option value="TImber/Bamboo-Mud">Madera/Barro y Bambú</option>
                    <option value="Timber-Planck">Tabla y madera</option>
                    <option value="RCC/RB/RBC">RCC/RB/RBC</option>
                  </Input>
                </FormGroup>
              </Col>
              <Col xs="6" style={campos}>
                <FormGroup check inline>
                  <Label style={label} check>
                    <Input
                      onChange={(e) => {
                        this.handleChange({
                          target: {
                            name: e.target.name,
                            value: e.target.value,
                          },
                        });
                      }}
                      type="checkbox"
                      name="has_superstructure_bamboo"
                      value={1}
                    /> Bambú
                  </Label>
                </FormGroup>
                <br />
                <br />
                <FormGroup check inline>
                  <Label style={label} check>
                    <Input
                      onChange={(e) => {
                        this.handleChange({
                          target: {
                            name: e.target.name,
                            value: e.target.value,
                          },
                        });
                      }}
                      type="checkbox"
                      name="has_superstructure_other"
                      value={1}
                    /> Otros
                  </Label>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col style={titulos}>
                Uso<hr></hr>
              </Col>
            </Row>
            <FormGroup row>
              <Label style={label} for="exampleSelect" sm={4}>Uso Secundario</Label>
              <Col sm={8}>
                <Input
                  onChange={this.handleSecondaryUse}
                  type="select"
                  id="exampleSelect"
                >
                  <option  >Sin uso secundario</option>
                  <option value="has_secondary_use_gov_office" >Oficinas gubernamentales</option>
                  <option value="has_secondary_use_agriculture" >Agricultura</option>
                  <option value="has_secondary_use_hotel" >Hotel</option>
                  <option value="has_secondary_use_rental" >Alquiler</option>
                  <option value="has_secondary_use_institution" >Institución</option>
                  <option value="has_secondary_use_school" >Escuela</option>
                  <option value="has_secondary_use_industry" >Indústria</option>
                  <option value="has_secondary_use_health_post" >Centro de salud</option>
                  <option value="has_secondary_use_use_police" >Policía</option>
                  <option value="has_secondary_use_other" >Otros</option>
                </Input>
              </Col>
            </FormGroup>
          </Form>
          <Row>
            <Col xs={{ size: '6', offset: 4 }} style={campos}>
              <Row>
                <Col xs="4" style={{ campos, marginRight: "9%" }}>
                  <button className="botonesMenu" onClick={this.calcularEstimacion}>Calcular</button>{' '}
                </Col>
                <Col xs="4" style={campos}>
                  <button className="botonesMenu" onClick={goMenu} >Cancelar</button>{' '}
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
        <br />
        {this.state.mostrar ?
          <Container style={containerResult}>
            <Row>
              <Col style={{marginTop:"1%"}} xs={{ size: '3', offset: 1 }}><p style={{label}}>Daño:</p></Col>
              <Col style={{marginTop:"1%"}} xs="3"><p style={{label}}>{this.state.estimation.Damage}</p></Col>
              <Col xs={{ size: '4', offset: 1 }}>
                <button style={{width:"80%"}} className="botonesMenu" onClick={refreshPage}>Nuevo Cálculo</button>{' '}
              </Col>
            </Row>
          </Container> : <></>}
      </div>
    )
  }
}

export default Formulario;