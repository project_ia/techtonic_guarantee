import './styles/Botonesmenu.css';
import React, { Component, useState } from 'react';
import { Row, Col, Container, Modal, ModalBody, ModalHeader, FormGroup } from 'reactstrap';
import { Link } from 'react-router-dom';
/*import iconoperfil from './img/iconoperfil.png';*/

class Consulta extends Component {
    constructor(props) {
        super(props);
        this.state = {
            llista: [],
            consulta_id: '',
            comentario: '',
            abierto: false,
        }
        this.cerrar = this.cerrar.bind(this);
    }

    cerrar() {
        this.setState({
            abierto: !this.state.abierto
        })
    }
    render() {
        function goCalcula() {
            window.location.href = 'https://tech-ai.herokuapp.com/calcula'
        }
        function goFormulario() {
            window.location.href = 'https://tech-ai.herokuapp.com/formulario'
        }
        
        function goConsulta(){
            window.location.href = 'https://tech-ai.herokuapp.com/consulta'
        }
        const toggle = () => this.cerrar();
        return (

            <>


                <br></br>
                <br></br>
                <br></br>
                <br></br>

                <center><h2></h2></center>
                <div style={{ padding: "15%"}}>

                    <button block onClick={() => this.cerrar()} style={{marginRight: "7%"}} className="botonesMenu">CALCULAR</button>{' '}
                    <button block onClick={goConsulta} className="botonesMenu">CONSULTAR</button>{' '}

                </div>
                <br></br>
                <br></br>
                <Modal isOpen={this.state.abierto} toggle={toggle}>
                    <ModalHeader toggle={toggle}> 
                        <h1 className="titulo">CALCULAR:</h1>
                    </ModalHeader>
                    <ModalBody>
                        <Container>
                            <Row>
                                <Col xs="6">
                                    <button className="botonesMenu" onClick={goFormulario}>Nuevo</button>
                                </Col>
                                <Col xs="6">
                                    <button className="botonesMenu" onClick={goCalcula}>Existente</button>
                                </Col>
                            </Row>
                        </Container>
                    </ModalBody>
                </Modal>
            </>
        )
    }
}
export default Consulta;


