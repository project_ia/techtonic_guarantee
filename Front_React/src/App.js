import React from "react";
import Vista from "./Vista";
import "./styles/App.css";
import Menu from './Menu';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import NavBar from './NavBar'
import Formulario from './Formulario';
import Calcula from './Calcula';
import Consulta from './Consulta';
function App() {
  const loginContainer = () => (
    <>
      <div className="App" >
        <div style={{background: "url('/img/globo.png')", backgroundSize: "100%", width: "100%",minHeight: "100vh", }}>

          <Route path="/" component={Vista} />

        </div>
      </div>
    </>)

  const appContainer = () => (
    <>
      <div className="App">
        
        <div style={{background: "url('/img/globo.png')", backgroundSize: "100%", width: "100%",minHeight: "100vh", }}>
          <div> <b><NavBar/></b></div>

          <Route path="/menu" component={() => <Menu clase='Boton' />} />

          <Route path="/formulario" component={Formulario} />
          
          <Route path="/calcula" component={Calcula} />

          <Route path="/consulta" component={Consulta} />

        </div>
      </div>
    </>)

  return (
    <>
      <Router>
        <Switch>
          <Route exact path="/" component={loginContainer} />
          <Route component={appContainer} />
        </Switch>
      </Router>
    </>
  );
}
export default App;