import React, { Component } from 'react';
import {Table,Button,Container} from 'reactstrap';


const API_URL = "https://techtonic-api.herokuapp.com/get/";

const HEADERS = new Headers({
    'Accept': 'application/json',
    'Content-Type': 'application/json'
});

class Consulta extends Component {
    constructor(props) {
        super(props);
        this.state = {
            llista: [],
            building_id: '',
            land_surface_condition: '',
            foundation_type:'',
            roof_type:'',
            ground_floor_type:'',
            other_floor_type:'',
            position:'',
            plan_configuration:'',
            has_superstructure_adobe_mud:'',
            has_superstructure_mud_mortar_stone:'',
            has_superstructure_stone_flag:'',
            has_superstructure_cement_mortar_stone:'',
            has_superstructure_mud_mortar_brick:'',
            has_superstructure_cement_mortar_brick:'',
            has_superstructure_timber:'',
            has_superstructure_bamboo:'',
            has_superstructure_rc_non_engineered:'',
            has_superstructure_rc_engineered:'',
            has_superstructure_other:'',
            has_secondary_use_agriculture:'',
            has_secondary_use_hotel:'',
            has_secondary_use_rental:'',
            has_secondary_use_institution:'',
            has_secondary_use_school:'',
            has_secondary_use_industry:'',
            has_secondary_use_health_post:'',
            has_secondary_use_gov_office:'',
            has_secondary_use_use_police:'',
            has_secondary_use_other:'',


        }
        this.cargaDatos = this.cargaDatos.bind(this);
        this.renderMaterialColumn = this.renderMaterialColumn.bind(this);
        this.renderUseColumn = this.renderUseColumn.bind(this);
    }

    componentDidMount() {
        this.cargaDatos();
    }
    cargaDatos() {
        const opcions = {
            method: "GET",
            headers: HEADERS
        };
        fetch(API_URL, opcions)
            .then(texto => texto.json())
            .then(datos => this.setState({ llista: datos }))
            .catch(error => console.log(error))
    }
    renderMaterialColumn(el) {
        const labels = ["Adobe Mud","Mud Mortar Stone","Stone flag","Cement Mortar Stone","Mud Mortar Brick","Cement Mortar Brick","Timber","Bamboo","RC Non Engineered","RC Engineered","Others"]

        const booleanos= [el.has_superstructure_adobe_mud,el.has_superstructure_mud_mortar_stone,
            el.has_superstructure_stone_flag,el.has_superstructure_cement_mortar_stone,
            el.has_superstructure_mud_mortar_brick,el.has_superstructure_cement_mortar_brick,
            el.has_superstructure_timber,el.has_superstructure_bamboo,el.has_superstructure_rc_non_engineered,
            el.has_superstructure_rc_engineered,el.has_superstructure_other]

        let listOfMaterial=""

        for (let [index, val] of booleanos.entries()){
            if (val){
                listOfMaterial+=labels[index]+"/"
            }
        }
        return listOfMaterial   
    }
    renderUseColumn(el) {
        const labels = ["Agriculture","Hotel","Rental","Institution","School","Industry","Health Post","Gov Office","Police","Others"]

        const booleanos= [el.has_secondary_use_agriculture,el.has_secondary_use_hotel,
            el.has_secondary_use_rental,el.has_secondary_use_institution,
            el.has_secondary_use_school,el.has_secondary_use_industry,
            el.has_secondary_use_health_post,el.has_secondary_use_gov_office,el.has_secondary_use_use_police,
            el.has_secondary_use_other]

        let listOfUse=""

        for (let [index, val] of booleanos.entries()){
            if (val){
                listOfUse+=labels[index]
            }
        }
        return listOfUse   
    }

    render() {
        function goMenu(){
            window.location.href = 'https://tech-ai.herokuapp.com/menu'
        }

        if (this.state.llista.length === 0) {
            return <h3>Cargando...</h3>
        }
        const filas = this.state.llista.map((el, i) => (
            <tr key={i}>
                <td>{el.building_id}</td>
                <td>{el.land_surface_condition}</td>
                <td>{el.foundation_type}</td>
                <td>{el.roof_type}</td>
                <td>{el.ground_floor_type}</td>
                <td>{el.other_floor_type}</td>
                <td>{el.position}</td>
                <td>{el.plan_configuration}</td>
                <td>{this.renderMaterialColumn(el)}</td>
                <td>{this.renderUseColumn(el)}</td>
            </tr>
        
        ));
        const campos_titulos={
            fontFamily: "monospace",
            color: "rgb(83, 83, 83)",
            letterSpacing: "1px",
            fontSize:"10px"
        }
        const container={
            backgroundColor: "white",
            marginTop: "101px",
            borderRadius: "10px",
            padding: "10px",
            
            

        }
        return (

            <div style={{ padding: "1%" }}>

                <Container style={container} fluid={true}>
                    <center><h2> Edificios </h2></center>
                    
                    <Table style={campos_titulos}>
                        <thead>
                            <tr>
                                <th>ID del edificio</th>
                                <th>Tipo Superfície</th>
                                <th>Cimientos</th>
                                <th>Tejado</th>
                                <th>Suelo</th>
                                <th>Otros Suelos</th>
                                <th>Posición</th>
                                <th>Plano de Configuración</th>
                                <th>Estructura</th>
                                <th>Uso Secundario</th>
                            
                            </tr>
                        </thead>
                        <tbody>
                            {filas}
                        </tbody>
                    </Table>
                    <button block className="botonesMenu" onClick={goMenu} color='info'>VOLVER</button>
                </Container>
            </div>
        )
    }
}
export default Consulta;