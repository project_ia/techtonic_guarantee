import React, { Component } from 'react';
import { Input,Container, Col, Label, Button, Form } from 'reactstrap';
import "./styles/App.css";


const API_URL = "https://techtonic-api.herokuapp.com/test/";


class Calcula extends Component {
    constructor(props) {
        super(props);
        this.state = {
            llista: [],
            id_edificio: '',
            esti: true

        }
        this.cargaDatos = this.cargaDatos.bind(this);
        this.guardarInputs = this.guardarInputs.bind(this);
    }

    cargaDatos() {
        console.log("cargaDatos")
        let url = API_URL + "?id_edificio=" + this.state.id_edificio;
        console.log(url)
        fetch(url)
            .then(texto => texto.json())
            .then(datos => this.setState({ llista: datos }, () => console.log('soy el console del fetch', this.state.llista)))
            .then(this.setState({ esti: false }))
            .catch(error => console.log("se ha producido un error: ", error))
    }

    guardarInputs(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    render() {

        function goMenu() {
            window.location.href = 'https://tech-ai.herokuapp.com/menu'
        }
        const inputCuadrado = {
            width: "20%",
            marginLeft: "6%",
            float:"left"
        }
        const h3 ={
            float:"left",
            marginLeft:"20%",
            fontFamily: "monospace",
            color: "rgb(83, 83, 83)",
        }
        const h32 ={
            fontFamily: "monospace",
            color: "rgb(83, 83, 83)",
        }
        const hr = {
            border: "0",
            width: "100%",
            color: "black",
            backgroundColor: "black",
            height: "5px"
          }
        const input ={
            flaot:"right",
            
        }
        const h3daño={
            float:"left",
            marginLeft: "3%",
            fontFamily: "monospace",
            color: "rgb(67, 98, 238)",
        }
        const container = {
            backgroundColor: "white",
            marginTop: "10px",
            borderRadius: "10px",
            padding: "10px",
            
      
          }
        return (
            <div style={{padding:"8%"}}>
                <Container style={container}>
                    <Col>
                        <div className="formulario" style={{ padding: "6%" }}>
                            <strong style={h32}className="texto">Consulta de edificio existente</strong>
                            <hr></hr>
                            <div>
                                <br></br>
                                <br></br>
                                <h3 className="texto" style={h3}>ID del edificio:</h3>
                                <Input stlye={input} name="id_edificio" onChange={this.guardarInputs} style={inputCuadrado}></Input><br /><br /> 
                            </div>
                            <div>
                                <h3 className="texto" style={h3}>Daño del edificio: </h3>
                                <h3 className="texto" style={h3daño}>{this.state.llista.Daño}</h3>
                            </div>
                        </div>
                        <div>
                            <br></br>
                            <br></br>
                            <button style={{marginRight: "2%"}} onClick={this.cargaDatos} className="botonesMenu">CALCULAR</button>
                            <button className="botonesMenu" onClick={goMenu} >VOLVER</button>{' '}

                        </div>
                    </Col>
                </Container>
            </div>
        )
    }
}
export default Calcula;