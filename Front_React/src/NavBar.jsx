import React from 'react';
import {
  Navbar,
  NavbarBrand,
  Nav,
} from 'reactstrap';

const NavBar = (props) => {
    function goInicio() {
      window.location.href = 'https://tech-ai.herokuapp.com/'
  }
  return (
    <div>
      <Navbar fixed='top' light expand="md" className="barra">
        <NavbarBrand><img className="logonavbar" onClick={goInicio} src={"/img/LOGOcab.png"} alt=''/></NavbarBrand>
          <Nav className="mr-auto" navbar></Nav>
         <NavbarBrand ><h1 className="texto">TECHTONIC GUARANTEE</h1></NavbarBrand>
      </Navbar>
    </div>
    );
  }

export default NavBar;
