from Database import Database
from mysql.connector import Error
import json


class BuildingAdmin():

    database = Database()

    def __init__ (self):
        pass

    def GetBuildingData(self,id):
    
        try:
            connection = self.database.CreateConnection()

            if connection != False:

                cursor = connection.cursor()

                cursor.execute(f'SELECT * FROM train_values WHERE building_id={id}')
                arrInitialData = cursor.fetchall()

                return arrInitialData

            else:
                exit()
        except Error as e:
            print('Error while fetching data: ', e)
            exit()
        finally:
            self.database.DestroyConnection(cursor, connection)
    
    def GetAllBuildings(self):
        
        try:
            connection = self.database.CreateConnection()

            if connection != False:

                cursor = connection.cursor()

                cursor.execute(f'SELECT * FROM train_values')
                row_headers=[x[0] for x in cursor.description]
                arrInitialData = cursor.fetchall()
                json_data=[]
                for result in arrInitialData:
                    json_data.append(dict(zip(row_headers,result)))
                return json.dumps(json_data)

            else:
                exit()
        except Error as e:
            print('Error while fetching data: ', e)
            exit()
        finally:
            self.database.DestroyConnection(cursor, connection)
