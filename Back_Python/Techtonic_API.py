import pandas as pd
import numpy as np
import pickle
import time
import json
from class_partial_ocurrencies import PartialOcurrencies
from class_totalOcurrencies import totalOcurrencies
from class_probabilidadWord import probabilidadWord
from class_wordinclass import TotalWord
from class_partialWords import PartialWords
from BuildingAdmin import BuildingAdmin
from class_estimationCategorical import EstimationCategorical
from class_probabilidadClase import ProbClass
from flask import Flask, jsonify,request as req
from flask_cors import CORS,cross_origin

########################################################################
############################Funciones###################################
########################################################################

##############################################################
##################Functions Use ##############################
##############################################################

def getPalabrasClaseByUse(data,listaClases,wordsObject):
    """ 
    Count the number of words in 'has secondary use'
    We need list of clases and the object to store the results
    """
    

    countWordsList=[0]*len(listaClases)

    for _, building in data.iterrows():
        
        num=[building.has_secondary_use_agriculture,
                building.has_secondary_use_hotel,building.has_secondary_use_rental,
                building.has_secondary_use_institution,building.has_secondary_use_school,
                building.has_secondary_use_industry,building.has_secondary_use_health_post,
                building.has_secondary_use_gov_office,building.has_secondary_use_use_police,
                building.has_secondary_use_other]

        for idx,clase in enumerate(listaClases):
            if building.damage_grade == clase:
                countWordsList[idx]+=np.sum(num)

    wordsObject.setWordsUse(countWordsList)
    
    return wordsObject

def getWordOcurrencesInClassByUse(data,listaClases,OcurrObject):
    """
    Compute the ocurrences of each word in 'has secondary use'
    We need the list of clases and the object to store the ocurrencies 
    """

    ocurrenciesClassMatrix=[[0]*10 for i in listaClases]

    for _, building in data.iterrows():

        num=[building.has_secondary_use_agriculture,
                building.has_secondary_use_hotel,building.has_secondary_use_rental,
                building.has_secondary_use_institution,building.has_secondary_use_school,
                building.has_secondary_use_industry,building.has_secondary_use_health_post,
                building.has_secondary_use_gov_office,building.has_secondary_use_use_police,
                building.has_secondary_use_other]

        for idx,clase in enumerate(listaClases):
            if building.damage_grade == clase:
                ocurrenciesClassMatrix[idx]= np.add(ocurrenciesClassMatrix[idx],num)
   
    OcurrObject.setWordsUse(ocurrenciesClassMatrix)
    
    return OcurrObject

    
##############################################################
##################Functions Structure ########################
##############################################################

def getPalabrasClaseByStructure(data,listaClases,wordsObject):
    """ 
    Count the number of words in each Building by Structure
    We need list of clases and the object to store the results
    """

    countWordsList=[0]*len(listaClases)
    
    for _, building in data.iterrows():
        
        num=[building.has_superstructure_adobe_mud,building.has_superstructure_mud_mortar_stone,
                 building.has_superstructure_stone_flag, building.has_superstructure_cement_mortar_stone,
                 building.has_superstructure_mud_mortar_brick, building.has_superstructure_cement_mortar_brick,
                 building.has_superstructure_timber,building.has_superstructure_bamboo, 
                 building.has_superstructure_rc_non_engineered, building.has_superstructure_rc_engineered,
                 building.has_superstructure_other]
        
        for idx,clase in enumerate(listaClases):
            if building.damage_grade == clase:
                countWordsList[idx]+=np.sum(num)
        
    wordsObject.setWordsStructure(countWordsList)

    return wordsObject

def getWordOcurrencesInClassByStructure(data,listaClases,OcurrObject):
    """ 
    Count the ocurrencies of each word in each Building by Structure
    We need list of clases and the object to store the results
    """
    ocurrenciesClassMatrix=[[0]*11 for i in listaClases]

    for _, building in data.iterrows():

        num=[building.has_superstructure_adobe_mud,building.has_superstructure_mud_mortar_stone,
                 building.has_superstructure_stone_flag, building.has_superstructure_cement_mortar_stone,
                 building.has_superstructure_mud_mortar_brick, building.has_superstructure_cement_mortar_brick,
                 building.has_superstructure_timber,building.has_superstructure_bamboo, 
                 building.has_superstructure_rc_non_engineered, building.has_superstructure_rc_engineered,
                 building.has_superstructure_other]

        for idx,clase in enumerate(listaClases):
            if building.damage_grade == clase:
                ocurrenciesClassMatrix[idx]= np.add(ocurrenciesClassMatrix[idx],num)
    
    OcurrObject.setWordsStructure(ocurrenciesClassMatrix)
    
    return OcurrObject

##############################################################
##################Functions Type #############################
##############################################################

def getPalabrasClaseByType(data,listaClases,words,wordsObject):
    """
    Compute the number of words in each Building by Type of structure
    We need the data and the object to store the results
    """
    countWordsList=[]

    clases=data['damage_grade']

    campos=data[['land_surface_condition','foundation_type','ground_floor_type','roof_type','other_floor_type','position','plan_configuration']]
    num=len(campos.columns)

    prob=clases.value_counts()

    for el in prob:
        wordNumber=el*num
        countWordsList.append(wordNumber)
    
    wordsObject.setWordsType(countWordsList)

    return wordsObject

def getWordOcurrencesInClassByType(data,listaClases,words,OcurrObject):
    """
    Compute the ocurrencies of each word for Building structure TYPE
    We need the clases list, the vocabulary and the object to store the results
    """

    ocurrenciesClassMatrix=[[0]*len(words) for i in listaClases]

    for _, building in data.iterrows():
        
        campos=[building.land_surface_condition,building.foundation_type,
                building.ground_floor_type,building.roof_type,
                building.other_floor_type,building.position,
                building.plan_configuration]
        
        for row,clase in enumerate(listaClases):
            if building.damage_grade == clase:
                for col,letra in enumerate(words):
                    if letra in campos:
                        num=campos.count(letra)
                        ocurrenciesClassMatrix[row][col]+=num
    #Guardamos como atributo de objeto
    OcurrObject.setWordsType(ocurrenciesClassMatrix)

    return OcurrObject

############################################################
#################### General Functions #####################
############################################################

def getClases(data):
    """
    Read the available clases from the data.
    """
    clases=data['damage_grade']
    classListTemp=[]
    for idx,clase in enumerate(clases):
        if not clase in classListTemp:
            classListTemp.append(clase)
    
    prob=clases.value_counts(classListTemp)
  
    classList=[]
    probClases=[]
    for idx,el in enumerate(prob):
        nameClass=prob.index[idx]
        classList.append(nameClass)
        probClassTemp=clases.value_counts(classListTemp)[idx]
        probClases.append(probClassTemp)
        
    return classList,probClases
    
def getVocabulario(data):
    """
    Read the different words of the data.
    They will become our vocabulary
    """
    vocabulary=[]
    for _, building in data.iterrows():
        campos=[building.land_surface_condition,building.foundation_type,
                building.foundation_type,building.roof_type,
                building.ground_floor_type,building.other_floor_type,
                building.other_floor_type,building.position,
                building.plan_configuration]
        for letra in campos:
            if not letra in vocabulary:
                vocabulary.append(letra)
    
    writePickleVocabulario(vocabulary)

    return vocabulary

def getWordProbabilityInClass(OcurrObject,probabilidadObj,WordsObj):
    """
    Build the final Matrix of Probability for each word in each class.
    It needs the ocurrencies of the words and the number of words.
    Lastly it needs an object to store the matrix.
    """

    matrixOcurrencies=OcurrObject.getTotalOcurrencies()

    probabilidades=[[0]*len(matrixOcurrencies[0]) for i in matrixOcurrencies]

    for row,clase in enumerate(matrixOcurrencies):
        for col,prob in enumerate(clase):
            probabilidades[row][col]=(prob+1)/(WordsObj.getTotal()[row]+len(clase))
    probabilidadObj.setProbabilidadWordVeryLow(probabilidades[4])
    probabilidadObj.setProbabilidadWordLow(probabilidades[3])
    probabilidadObj.setProbabilidadWordMedium(probabilidades[2])
    probabilidadObj.setProbabilidadWordHigh(probabilidades[1])
    probabilidadObj.setProbabilidadWordVeryHigh(probabilidades[0])

    return probabilidadObj

def getOcurrencies(datos,listaClases,words,OcurrObject):
    """
    Call the functions to compute the ocurrencies for the different types of data.
    It returns an object with the ocurrencies for each data type.
    This will be Partial Ocurrencies
    """
    
    ocurrencies = getWordOcurrencesInClassByType(datos,listaClases,words,OcurrObject)
    ocurrencies = getWordOcurrencesInClassByUse(datos,listaClases,OcurrObject)
    ocurrencies = getWordOcurrencesInClassByStructure(datos,listaClases,OcurrObject)
    
    return ocurrencies

def getFinalOcurrencies(ocurrenciesPartial,totalOcurrObject):
    """
    It concatenates the Partial Ocurrencies and returns a single list with all the ocurrencies
    It returns an object with a matrix, each row are the ocurrencies in one class.
    """

    totalOcurrMatrix=[]
    ocurrUse=ocurrenciesPartial.getOcurrUse()
    ocurrType=ocurrenciesPartial.getOcurrType()
    ocurrStructure=ocurrenciesPartial.getOcurrStructure()
    for idx,row in enumerate(ocurrUse):
        totalOcurrMatrix.append(
            np.concatenate(
                (ocurrType[idx],ocurrStructure[idx],ocurrUse[idx])
                )
            )
    
    totalOcurrObject.setTotalOcurrencies(totalOcurrMatrix)

    return totalOcurrObject

def getWords(datos,listaClases,vocabulario,wordsObject):
    """
    Call the functions to compute the numbers of words in each class.
    Returns an object with all the words from each data type.
    """
    words = getPalabrasClaseByType(datos,listaClases,vocabulario,wordsObject)
    words = getPalabrasClaseByUse(datos,listaClases,wordsObject)
    words = getPalabrasClaseByStructure(datos,listaClases,wordsObject)
    return words

def totalWords(partialWordObject,WordObject):
    """
    Adds the total number of words in each class.
    """

    total=[]

    wordsUse=partialWordObject.getWordsUse()
    wordsStructure=partialWordObject.getWordsStructure()
    wordsType=partialWordObject.getWordsType()
    for idx, words in enumerate(wordsUse):
        total.append(wordsUse[idx]+wordsStructure[idx]+wordsType[idx])

    WordObject.setTotal(total)

    return WordObject

#Functions to Read/Write Files

def leerDatos():
    """
    Read the data form a .csv file
    """
    valores = pd.read_csv('../datos/original_data.csv')
    clases = pd.read_csv('../datos/use_data.csv')
    datos= pd.merge(valores,clases)
    datos=datos.dropna()
    return datos

def writePickleVocabulario(vocabulario):
    with open('vocabulario', 'wb') as vocabulario_file:
        pickle.dump(vocabulario,vocabulario_file)

def readPickleVocabulario():
    with open('vocabulario', 'rb') as vocabulario_file:
        config_vocabulario = pickle.load(vocabulario_file)
    return config_vocabulario

def writePickleProbLleno(probabilidadLleno):
    with open('probabilidadLleno.save', 'wb') as probabilidadLleno_file:
        pickle.dump(probabilidadLleno, probabilidadLleno_file)

def readProbabilidad ():
    with open('./probabilidadLleno.save', 'rb') as config_probabilidad_file:
        probabilidad = pickle.load(config_probabilidad_file)
    return probabilidad

def writePickleProbClase(probabilidadClases):
    with open('probabilidadClases.save', 'wb') as probabilidadClases_file:
        pickle.dump(probabilidadClases, probabilidadClases_file)

def readProbabilidadClases ():
    with open('./probabilidadClases.save', 'rb') as config_probabilidad_file:
        probabilidadClase = pickle.load(config_probabilidad_file)
    return probabilidadClase

def writePickleListaClase(listaClases):
    with open('listaClases.save', 'wb') as listaClases_file:
        pickle.dump(listaClases, listaClases_file)

def readListaClases ():
    with open('./listaClases.save', 'rb') as config_lista_file:
        listaClase = pickle.load(config_lista_file)
    return listaClase

###########################################################################
##################### Main Function TRAIN #################################
###########################################################################

app=Flask(__name__)
cors=CORS(app)

@app.route('/train/', methods=['GET'])
@cross_origin()
def train():

    #To compute the runtime of the train
    start=time.time()

    data=leerDatos()
    print('Lei data')

    #Instances of Objects
    ocurrencies=PartialOcurrencies()
    totalOcurr=totalOcurrencies()
    probabilidadObj = probabilidadWord()
    wordEmpty = TotalWord()
    partialWordsEmpty = PartialWords()

    #ALGORITHM
    
    #Obtain the Clases and Vocabulary

    listaClases,probClass=getClases(data)

    writePickleListaClase(listaClases)

    probabilidadClases=ProbClass(probClass)

    writePickleProbClase(probabilidadClases)

    vocabulario=getVocabulario(data)
    print('Obtuve clases y vocabulario')

    #Count the number fo words

    partialWords=getWords(data,listaClases,vocabulario,partialWordsEmpty)
    
    wordsFull=totalWords(partialWords,wordEmpty)
    print('Finished the Count of the Total Words')

    #Count the ocurrencies of the WOrds in each Class

    ocurrenciesPartial=getOcurrencies(data,listaClases,vocabulario,ocurrencies)
    print('Partiacl Ocurrencies: Done')
    finalOcurrencies=getFinalOcurrencies(ocurrenciesPartial,totalOcurr)
    print('Total Ocurrencies: Done')
    
    #Calculate the Probabilities for each word in each Class
    probabilidadLleno = getWordProbabilityInClass(finalOcurrencies,probabilidadObj,wordsFull)

    writePickleProbLleno(probabilidadLleno)

    #Final Runtime
    end=time.time()
    runtime=time.strftime('%H:%M:%S',time.gmtime(end-start))
    print(f"Runtime is {runtime}")

    return jsonify({"Status":"Train Done","Runtime:":runtime})

###########################################################################
###################Functions ESTIMATION ###################################
###########################################################################


def getCountWordInTest(building,words):
    """
    Count the number of words in the input Building
    """
    countWordsBuilding=[0]*len(words)
    for idx,palabra in enumerate(words):
        num=0
        if palabra in building[0]:
            num=building[0].count(palabra)
            countWordsBuilding[idx]=num+countWordsBuilding[idx]
    return countWordsBuilding

@app.route('/test/', methods=['GET'])
@cross_origin()
def getEstimation():
    """
    Get estimation from Building in existing Data Base
    """
    myBuilding = req.args.get('id_edificio')

    #Read Probabilities, Vocabulary, Class Probability and Class List
    objectProbability=readProbabilidad()
    vocabulario=readPickleVocabulario()
    probClases=readProbabilidadClases ()
    listaClases=readListaClases()

    #Instance of Data Base Admin
    BuildingAd=BuildingAdmin()

    #Read the Building from Data Base
    building=BuildingAd.GetBuildingData(myBuilding)
    
    countWordsBuilding=getCountWordInTest(building,vocabulario)
    buildingBoolean=building[0][8:]

    buildingArray=tuple(countWordsBuilding)+buildingBoolean
    estimacionVeryLow=probClases.getProbVeryLow()
    estimacionLow=probClases.getProbLow()
    estimacionMedium=probClases.getProbMedium()
    estimacionHigh=probClases.getProbHigh()
    estimacionVeryHigh=probClases.getProbVeryHigh()

    for idx,param in enumerate(buildingArray):
        estimacionVeryLow=estimacionVeryLow*objectProbability.getProbabilidadWordVeryLow()[idx]**param
        estimacionLow=estimacionLow*objectProbability.getProbabilidadWordLow()[idx]**param
        estimacionMedium=estimacionMedium*objectProbability.getProbabilidadWordMedium()[idx]**param
        estimacionHigh=estimacionHigh*objectProbability.getProbabilidadWordHigh()[idx]**param
        estimacionVeryHigh=estimacionVeryHigh*objectProbability.getProbabilidadWordVeryHigh()[idx]**param
    
    probabilidadArray=(estimacionVeryLow,estimacionLow,estimacionMedium,estimacionHigh,estimacionVeryHigh)

    for idx, prob in enumerate(probabilidadArray):
        tempList=[x for i,x in enumerate(probabilidadArray) if i!=idx] 
        if all(number<prob for number in tempList):
            return jsonify({'Damage':listaClases[idx],'ID_Edificio':myBuilding})

@app.route('/calcular/', methods=['POST'])
@cross_origin()
def calcularNewBuilding():
    """
    Get the Damage Estimation of a new Building from the form
    """
   
    #Read Probabilities, Vocabulary, Class Probability and Class List
    objectProbability=readProbabilidad()
    vocabulario=readPickleVocabulario()
    probClases=readProbabilidadClases ()
    listaClases=readListaClases()

    #Read and format the new Building
    building=req.json
    arrayBuilding=[building['land_surface_condition'],building['foundation_type'],
                building['ground_floor_type'],building['roof_type'],
                building['other_floor_type'],building['position'],
                building['plan_configuration'],int(building['has_superstructure_adobe_mud']),
                int(building['has_superstructure_mud_mortar_stone']),int(building['has_superstructure_stone_flag']), 
                int(building['has_superstructure_cement_mortar_stone']), int(building['has_superstructure_mud_mortar_brick']), 
                int(building['has_superstructure_cement_mortar_brick']), building['has_superstructure_timber'],
                int(building['has_superstructure_bamboo']), int(building['has_superstructure_rc_non_engineered']),
                int(building['has_superstructure_rc_engineered']), building['has_superstructure_other'],
                int(building['has_secondary_use_agriculture']),
                int(building['has_secondary_use_hotel']),int(building['has_secondary_use_rental']),
                int(building['has_secondary_use_institution']),int(building['has_secondary_use_school']),
                int(building['has_secondary_use_industry']),int(building['has_secondary_use_health_post']),
                int(building['has_secondary_use_gov_office']),int(building['has_secondary_use_use_police']),
                int(building['has_secondary_use_other'])]
    
    #Compute
    countWordsBuilding=getCountWordInTest(arrayBuilding,vocabulario)
    buildingBoolean=arrayBuilding[8:]
    buildingArray=countWordsBuilding+buildingBoolean
    estimacionVeryLow=probClases.getProbVeryLow()
    estimacionLow=probClases.getProbLow()
    estimacionMedium=probClases.getProbMedium()
    estimacionHigh=probClases.getProbHigh()
    estimacionVeryHigh=probClases.getProbVeryHigh()

    for idx,param in enumerate(buildingArray):
        estimacionVeryLow=estimacionVeryLow*objectProbability.getProbabilidadWordVeryLow()[idx]**param
        estimacionLow=estimacionLow*objectProbability.getProbabilidadWordLow()[idx]**param
        estimacionMedium=estimacionMedium*objectProbability.getProbabilidadWordMedium()[idx]**param
        estimacionHigh=estimacionHigh*objectProbability.getProbabilidadWordHigh()[idx]**param
        estimacionVeryHigh=estimacionVeryHigh*objectProbability.getProbabilidadWordVeryHigh()[idx]**param
    
    probabilidadArray=(estimacionVeryLow,estimacionLow,estimacionMedium,estimacionHigh,estimacionVeryHigh)

    for idx, prob in enumerate(probabilidadArray):
        tempList=[x for i,x in enumerate(probabilidadArray) if i!=idx] 
        if all(number<prob for number in tempList):
            return jsonify({'Damage':listaClases[idx]})

@app.route('/get/', methods=['GET'])
@cross_origin()
def getBuildings():
    """
    SQL query to read all the Buildings existing in the Data Base
    """
    #Instanciar Objeto Conectar BDD
    BuildingAd=BuildingAdmin()

    buildings=BuildingAd.GetAllBuildings()

    return buildings