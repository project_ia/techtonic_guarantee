class EstimationCategorical():
    
    def __init__(self,low,medium,high):
        self.setEstimationLow(low)
        self.setEstimationMedium(medium)
        self.setEstimationHigh(high)
    
    def setEstimationLow(self,low):
        self.__estimationLow=low
    
    def setEstimationMedium(self,medium):
        self.__estimationMedium=medium
    
    def setEstimationHigh(self,high):
        self.__estimationHigh=high
    
    def getEstimationLow(self):
        return self.__estimationLow
    
    def getEstimationMedium(self):
        return self.__estimationMedium
    
    def getEstimationHigh(self):
        return self.__estimationHigh