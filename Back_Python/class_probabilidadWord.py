class probabilidadWord():
    def setProbabilidadWordVeryLow (self, probabilidadWordVeryLow):
        self.probabilidadWordVeryLow = probabilidadWordVeryLow
    
    def setProbabilidadWordLow (self, probabilidadWordLow):
        self.probabilidadWordLow = probabilidadWordLow

    def setProbabilidadWordMedium (self, probabilidadWordMedium):
        self.probabilidadWordMedium = probabilidadWordMedium
    
    def setProbabilidadWordHigh (self, probabilidadWordHigh):
        self.probabilidadWordHigh = probabilidadWordHigh

    def setProbabilidadWordVeryHigh (self, probabilidadWordVeryHigh):
        self.probabilidadWordVeryHigh = probabilidadWordVeryHigh

    def getProbabilidadWordVeryLow (self): 
        return self.probabilidadWordVeryLow 

    def getProbabilidadWordLow (self): 
        return self.probabilidadWordLow 
    
    def getProbabilidadWordMedium (self): 
        return self.probabilidadWordMedium 
    
    def getProbabilidadWordHigh (self):
        return self.probabilidadWordHigh

    def getProbabilidadWordVeryHigh (self):
        return self.probabilidadWordVeryHigh
        
