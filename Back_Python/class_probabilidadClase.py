class ProbClass():

    def __init__(self,probList):
        self.setProbVeryHigh(probList[0])
        self.setProbHigh(probList[1])
        self.setProbMedium(probList[2])
        self.setProbLow(probList[3])
        self.setProbVeryLow(probList[4])

    def setProbVeryLow (self, ProbVeryLow):
        self.ProbVeryLow = ProbVeryLow

    def setProbLow (self, ProbLow):
        self.ProbLow = ProbLow

    def setProbMedium (self, ProbMedium):
        self.ProbMedium = ProbMedium

    def setProbHigh (self, ProbHigh):
        self.ProbHigh = ProbHigh

    def setProbVeryHigh (self, ProbVeryHigh):
        self.ProbVeryHigh = ProbVeryHigh

    def getProbVeryLow (self): 
        return self.ProbVeryLow 

    def getProbLow (self): 
        return self.ProbLow 

    def getProbMedium (self): 
        return self.ProbMedium 
    
    def getProbHigh (self):
        return self.ProbHigh

    def getProbVeryHigh(self):
        return self.ProbVeryHigh
          