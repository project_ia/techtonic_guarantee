import mysql.connector
from mysql.connector import Error

class Database():

    def __init__(self):
        pass

    def CreateConnection (self):
        try:
            host = 'eu-cdbr-west-03.cleardb.net'
            db = 'heroku_2980b221913e582'
            pwd = 'a194665f'
            user = 'bd6afe9396a8a8'

            connection = mysql.connector.connect(host=host, database=db, user=user, password=pwd, auth_plugin='mysql_native_password')
            
            if connection.is_connected():
                return connection

        except Error as err:
            print("Error while connecting to MySQL: ", err)
            return False

    def DestroyConnection (self, cursor, connection):

        if (connection.is_connected()):
            cursor.close()
            connection.close()