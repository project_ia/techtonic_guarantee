class PartialWords():

    def setWordsUse(self,wordsList):
        self.__wordsUse=wordsList
    
    def setWordsStructure(self,wordsList):
        self.__wordsStructure=wordsList
    
    def setWordsType(self,wordsList):
        self.__wordsType=wordsList
    
    def getWordsUse(self):
        return self.__wordsUse
    
    def getWordsStructure(self):
        return self.__wordsStructure
    
    def getWordsType(self):
        return self.__wordsType
    